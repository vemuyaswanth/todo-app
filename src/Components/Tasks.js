import React from 'react'
import Task from './BasicComponents/Task';
import './ComponentCss/Tasks.css'

const Tasks = ({ section }) => {
  return (
    <>
      <div className="tasks-list">
        <Task />
      </div>
      <div className="add-tasks">
        <input type="text" name="add-tasks" id="tasks-input" placeholder={`Enter a tasks to add in ${section}`} />
      </div>
    </>
  )
}

export default Tasks
