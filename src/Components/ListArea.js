import React from 'react'
import Header from './Header'
import './ComponentCss/ListArea.css'
import Tasks from './Tasks'

const ListArea = ({ section }) => {
  return (
    <div className="list-area-root">
    <Header section={section} />
    <div className="list-area">
      <Tasks section={section}/>
    </div>
    </div>
  )
}

export default ListArea
