import React from 'react'

const Task = () => {
  
  const changed = (task) => {
    console.log(task.target);
  }
  
  return (
    <div className="task">
      <input type="checkbox" name="status" class="status-checkbox" taskId='thisIsTheID' onChange={e => changed(e)} />
      <p className="task-name">This is a Task that was added.</p>
    </div>
  )
}

export default Task
